%if 0%{?fedora} > 12
%global with_python3 1
%else
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python2_sitearch: %global python2_sitearch %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

%global srcname mo_pack
%global commit 0ab6d56ce40d1c799a7f335a17dd0495cee311c4
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:		python-%{srcname}
Version:	0.2.0
Release:	2%{?dist}
Summary:	A Python library for packing UM data payloads

License:	LGPLv3
URL:		https://github.com/SciTools/mo_pack
Source0:	https://github.com/SciTools/mo_pack/archive/%{commit}/%{name}-%{commit}.tar.gz


BuildRequires:	mo_unpack-devel >= 3

BuildRequires:	python2-devel
BuildRequires:	python2-setuptools
BuildRequires:	Cython
%if 0%{?fedora} > 23
BuildRequires:	python2-numpy
%else
BuildRequires:	numpy
%endif
BuildRequires:	python2-nose

%if 0%{?with_python3}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-Cython
BuildRequires:	python3-numpy
BuildRequires:	python3-nose
%endif  # if with_python3


%description
A python module containing packing methods used to encode and decode the data
payloads of UM "fields".

Supports WGDOS and RLE encoding methods.


%package -n python2-%{srcname}
Summary:	A Python library for packing UM data payloads
%{?python_provide:%python_provide python2-%{srcname}}

%if 0%{?fedora} > 23
Requires:	python2-numpy
%else
Requires:	numpy
%endif

%description -n python2-%{srcname}
A python module containing packing methods used to encode and decode the data
payloads of UM "fields".

Supports WGDOS and RLE encoding methods.


%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:	A Python library for packing UM data payloads
%{?python_provide:%python_provide python3-%{srcname}}

Requires:	python3-numpy

%description -n python3-%{srcname}
A python module containing packing methods used to encode and decode the data
payloads of UM "fields".

Supports WGDOS and RLE encoding methods.
%endif  # with_python3


%prep
%autosetup -n %{srcname}-%{commit}


%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif  # with_python3


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python3 version
# to be the default).
%py2_install
%if 0%{?with_python3}
%py3_install
%endif # with_python3


%check
mkdir test_folder
pushd test_folder
PYTHONPATH="%{buildroot}%{python2_sitearch}" \
	nosetests-%{python2_version} --with-doctest -sv mo_pack

%if 0%{?with_python3}
PYTHONPATH="%{buildroot}%{python3_sitearch}" \
	nosetests-%{python3_version} --with-doctest -sv mo_pack
%endif
popd


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%doc README.md
%license COPYING COPYING.LESSER
%{python2_sitearch}/mo_pack*


%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README.md
%license COPYING COPYING.LESSER
%{python3_sitearch}/mo_pack*
%endif # with_python3


%changelog
* Tue Oct 25 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.2.0-2
- Fix runtime dependencies
- Fix numpy BR on Fedora 23 and EL7

* Tue Oct 25 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.2.0-1
- Initial RPM release
