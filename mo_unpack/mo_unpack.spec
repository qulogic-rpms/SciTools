%global commit 54c6df7eb3c8e9d6abf3de217ed7732fcda05139
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:		mo_unpack
Version:	3.1.2
Release:	1%{?dist}
License:	BSD
Summary:	Met Office PP-Unpacking Code
Url:		https://github.com/SciTools/libmo_unpack
Group:		Development/Libraries
Source0:	https://github.com/SciTools/libmo_unpack/archive/%{commit}/%{name}-%{commit}.tar.gz
Patch1:		%{name}-Fix-format-warnings.patch
Patch2:		%{name}-Enable-install-in-GNU-directories.patch

BuildRequires:	cmake
BuildRequires:	check-devel

%description
A library for handling the WGDOS and RLE compression schemes used in UM files.


%package devel
Summary:	Met Office PP-Unpacking Development Files
Requires:	%{name} = %{version}

%description devel
Development files for %{name} library.


%prep
%setup -q -n lib%{name}-%{commit}

%patch1 -p1
%patch2 -p1

find . -name '*.txt' -executable | xargs chmod -x

%build
%cmake .
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%check
ctest -V %{?_smp_mflags}


%files
%{_libdir}/libmo_unpack.so.*

%files devel
%doc Document.txt
%{_includedir}/logerrors.h
%{_includedir}/rlencode.h
%{_includedir}/wgdosstuff.h
%{_libdir}/libmo_unpack.so

%changelog
* Tue Oct 25 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.1.2-1
- New upstream release

* Mon Mar 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.3-3
- Rebuild for Fedora 24

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.3-2
- Rebuild for Rawhide

* Wed Jul 23 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.3-1
- Initial package release
