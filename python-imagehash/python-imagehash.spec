%if 0%{?fedora}
%global with_python3 1
%endif

%global pypi_name ImageHash
%global srcname imagehash

Name:		python-%{srcname}
Version:	3.4
Release:	2%{?dist}
Summary:	A Python Perceptual Image Hashing Module

License:	BSD
URL:		https://github.com/JohannesBuchner/imagehash
Source0:	https://files.pythonhosted.org/packages/source/%(c=%{commit}; echo ${c:0:1})/%{pypi_name}/%{pypi_name}-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:	python2-devel
BuildRequires:	python%{?fedora:2}-setuptools
BuildRequires:	python2-six

# Runtime
%if 0%{?fedora}
BuildRequires:	python2-numpy
BuildRequires:	python2-scipy
%else
BuildRequires:	numpy
BuildRequires:	scipy
%endif
BuildRequires:	python%{?fedora:2}-pillow
BuildRequires:	python%{?fedora:2}-pywt


# Python 3
%if 0%{?with_python3}
BuildRequires:	python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-setuptools
BuildRequires:	python%{python3_pkgversion}-six
BuildRequires:	python%{python3_pkgversion}-numpy
BuildRequires:	python%{python3_pkgversion}-scipy
BuildRequires:	python%{python3_pkgversion}-pillow
BuildRequires:	python%{python3_pkgversion}-pywt
%endif # with_python3


%description
A image hashing library written in Python. ImageHash supports:
    average hashing (aHash)
    perception hashing (pHash)
    difference hashing (dHash)
    wavelet hashing (wHash)


%package -n python2-%{srcname}
Summary:	A Python Perceptual Image Hashing Module
%{?python_provide:%python_provide python2-%{srcname}}

Requires:	python2-six
%if 0%{?fedora}
Requires:	python2-numpy
Requires:	python2-scipy
%else
Requires:	numpy
Requires:	scipy
%endif
Requires:	python%{?fedora:2}-pillow
Requires:	python%{?fedora:2}-pywt

%description -n python2-%{srcname}
A image hashing library written in Python. ImageHash supports:
    average hashing (aHash)
    perception hashing (pHash)
    difference hashing (dHash)
    wavelet hashing (wHash)


%if 0%{?with_python3}
%package -n python%{python3_pkgversion}-%{srcname}
Summary:	A Python Perceptual Image Hashing Module
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

Requires:	python%{python3_pkgversion}-six
Requires:	python%{python3_pkgversion}-numpy
Requires:	python%{python3_pkgversion}-scipy
Requires:	python%{python3_pkgversion}-pillow
Requires:	python%{python3_pkgversion}-pywt

%description -n python%{python3_pkgversion}-%{srcname}
A image hashing library written in Python. ImageHash supports:

    average hashing (aHash)
    perception hashing (pHash)
    difference hashing (dHash)
    wavelet hashing (wHash)
%endif # with_python3


%prep
%autosetup -n %{pypi_name}-%{version}


%build
%py2_build
%if 0%{?with_python3}
%py3_build
%endif # with_python3



%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
%py2_install
%if 0%{?with_python3}
%py3_install
%endif # with_python3


%check
%{__python2} setup.py test
%if 0%{?with_python3}
%{__python3} setup.py test
%endif # with_python3


%files -n python2-%{srcname}
%doc README.rst
%license LICENSE
%{python2_sitelib}/imagehash
%{python2_sitelib}/ImageHash*
%if 0%{?!with_python3}
%{_bindir}/find_similar_images.py
%endif

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-%{srcname}
%doc README.rst
%license LICENSE
%{_bindir}/find_similar_images.py
%{python3_sitelib}/imagehash
%{python3_sitelib}/ImageHash*
%endif # with_python3


%changelog
* Thu Mar 02 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 3.4-2
- Add new six dependency

* Thu Mar 02 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 3.4-1
- New upstream release

* Tue Oct 25 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 3.0-1
- Initial RPM release
