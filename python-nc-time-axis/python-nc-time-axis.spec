%global srcname nc-time-axis
%global srcname_ nc_time_axis
%global sum Support for netcdftime axis in matplotlib

%global commit 1021ea3a3043eda0a35a10a992c068d91a62f7dd
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           python-%{srcname}
Version:        1.0.0
Release:        2%{?dist}
Summary:        %{sum}

License:        BSD
URL:            https://github.com/SciTools/nc-time-axis
Source0:        https://github.com/SciTools/%{srcname}/archive/%{commit}/python-%{srcname}-%{commit}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-setuptools python3-setuptools

# Runtime
BuildRequires:  python2-six python3-six
BuildRequires:  python2-numpy python3-numpy
BuildRequires:  python2-matplotlib python3-matplotlib
%if 0%{?fedora} > 25
BuildRequires:  python2-netcdf4 >= 1.1, python3-netcdf4 >= 1.1
%else
BuildRequires:  netcdf4-python >= 1.1, netcdf4-python3 >= 1.1
%endif

# Test only
BuildRequires:  python2-mock python3-mock
BuildRequires:  python-pep8 python3-pep8


%description
Support for netcdftime axis in matplotlib.


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-six
Requires:       python2-numpy
Requires:       python2-matplotlib
%if 0%{?fedora} > 25
Requires:       python2-netcdf4 >= 1.1
%else
Requires:       netcdf4-python >= 1.1
%endif

%description -n python2-%{srcname}
Support for netcdftime axis in matplotlib.


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-six
Requires:       python3-numpy
Requires:       python3-matplotlib
%if 0%{?fedora} > 25
Requires:       python3-netcdf4 >= 1.1
%else
Requires:       netcdf4-python3 >= 1.1
%endif

%description -n python3-%{srcname}
Support for netcdftime axis in matplotlib.


%prep
%autosetup -n %{srcname}-%{commit}


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
%{__python2} setup.py test
%{__python3} setup.py test


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname_}
%{python2_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname_}
%{python3_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%changelog
* Tue Mar 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-2
- Resync with latest Python spec template
- Update netCDF4 dependency on Fedora > 26

* Mon Oct 10 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-1
- Initial RPM release
