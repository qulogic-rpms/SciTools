%global srcname iris-sample-data
%global commit 83651bdaa4215e3754bef60709c942b803b677f3
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:		python-%{srcname}
Version:	2.0.0
Release:	2%{?dist}
Summary:	Sample data for python-iris

License:	OGL
URL:		http://scitools.org.uk/iris/
Source0:	https://github.com/SciTools/%{srcname}/archive/%{commit}/python-%{srcname}-%{commit}.tar.gz

BuildArch:	noarch
BuildRequires:	python2-devel python%{python3_pkgversion}-devel
BuildRequires:	python%{?fedora:2}-setuptools python%{python3_pkgversion}-setuptools


%description
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics.

This package contains sample data for the Iris library.


%package -n python2-%{srcname}
Summary:	Sample data for python2-iris
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics.

This package contains sample data for the Iris library.


%package -n python%{python3_pkgversion}-%{srcname}
Summary:	Sample data for python%{python3_pkgversion}-iris
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-%{srcname}
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics.

This package contains sample data for the Iris library.


%prep
%autosetup -n %{srcname}-%{commit}


%build
%py2_build
%py3_build



%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
%py2_install
%py3_install


%files -n python2-%{srcname}
%doc README.rst
%{python2_sitelib}/*

%files -n python%{python3_pkgversion}-%{srcname}
%doc README.rst
%{python3_sitelib}/*


%changelog
* Mon Oct 10 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.0.0-2
- Fix some typos in previous build
- Fix build on EL7

* Mon Oct 10 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.0.0-1
- New upstream release
- Switch to new Python specfile template

* Mon Mar 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.0-2
- Stop downloading source during build

* Wed Jul 23 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.0-1
- Initial RPM release
