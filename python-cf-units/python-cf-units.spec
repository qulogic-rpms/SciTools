%global srcname cf-units
%global srcname_ cf_units
%global sum Climate and Forecast (CF) convention units of measure

Name:           python-%{srcname}
Version:        1.1.3
Release:        2%{?dist}
Summary:        %{sum}

License:        LGPLv3
URL:            http://scitools.org.uk/cf_units/
Source0:        https://files.pythonhosted.org/packages/source/c/%{srcname_}/%{srcname_}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel python3-devel
BuildRequires:  python-setuptools python3-setuptools
BuildRequires:  python-six python3-six
BuildRequires:  python2-numpy >= 1.6.0, python3-numpy >= 1.6.0
%if 0%{?fedora} > 25
BuildRequires:  python2-netcdf4 >= 0.9.9, python3-netcdf4 >= 0.9.9
%else
BuildRequires:  netcdf4-python >= 0.9.9, netcdf4-python3 >= 0.9.9
%endif
BuildRequires:  udunits2-devel >= 2.1.24

BuildRequires:  python2-pytest python3-pytest
BuildRequires:  python-pep8 >= 1.4.6, python3-pep8 >= 1.4.6

%description
Provision of a wrapper class to support Unidata/UCAR UDUNITS-2, and the
netcdftime calendar functionality.


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-six
Requires:       python2-numpy
%if 0%{?fedora} > 25
Requires:       python2-netcdf4 >= 0.9.9
%else
Requires:       netcdf4-python >= 0.9.9
%endif

%description -n python2-%{srcname}
Provision of a wrapper class to support Unidata/UCAR UDUNITS-2, and the
netcdftime calendar functionality.


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-six
Requires:       python3-numpy
%if 0%{?fedora} > 25
Requires:       python3-netcdf4 >= 0.9.9
%else
Requires:       netcdf4-python3 >= 0.9.9
%endif

%description -n python3-%{srcname}
Provision of a wrapper class to support Unidata/UCAR UDUNITS-2, and the
netcdftime calendar functionality.


%prep
%autosetup -n %{srcname_}-%{version}


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install

rm -r %{buildroot}%{_docdir}/%{srcname_}


%check
py.test-%{python3_version} --pyargs cf_units
py.test-%{python2_version} --pyargs cf_units


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license COPYING COPYING.LESSER
%doc README.rst CHANGES
%{python2_sitelib}/%{srcname_}
%{python2_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license COPYING COPYING.LESSER
%doc README.rst CHANGES
%{python3_sitelib}/%{srcname_}
%{python3_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%changelog
* Tue Mar 7 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1.3-2
- Refresh from current Python spec template
- Update netCDF4 dependency on Fedora > 26

* Thu Mar 2 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1.3-1
- New upstream release

* Sat Jul 2 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1.1-2
- Fix BR for EPEL 7

* Sat Jul 2 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1.1-1
- Initial RPM release
