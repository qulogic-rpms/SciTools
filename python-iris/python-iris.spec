%global srcname iris
%global sum A Python library for Meteorology and Climatology

%global iris_sitecfg lib/iris/etc/site.cfg

Name:           python-%{srcname}
Version:        1.12.0
Release:        1%{?dist}
Summary:        %{sum}

License:        LGPLv3
URL:            http://scitools.org.uk/iris/
Source0:        https://github.com/SciTools/%{srcname}/archive/%{version}/%{srcname}-%{version}.tar.gz


BuildRequires:  python-iris-test-data >= 1.12.0
BuildRequires:  python-iris-sample-data >= 2.0.0

BuildRequires:  python2-devel python3-devel
BuildRequires:  python-setuptools python3-setuptools
%if 0%{?fedora} > 25
BuildRequires:  python2-netcdf4 >= 0.9.9, python3-netcdf4 >= 0.9.9
%else
BuildRequires:  netcdf4-python >= 0.9.9, netcdf4-python3 >= 0.9.9
%endif
BuildRequires:  python2-numpy >= 1.6.0, python3-numpy >= 1.6.0
BuildRequires:  pyke >= 1.1.1, python3-pyke >= 1.1.1
BuildRequires:  python-biggus >= 0.14.0, python3-biggus >= 0.14.0
BuildRequires:  python-cartopy >= 0.11.0, python3-cartopy >= 0.11.0
BuildRequires:  python-cf-units >= 1.0.0, python3-cf-units >= 1.0.0
BuildRequires:  python-matplotlib >= 1.3.1, python3-matplotlib >= 1.3.1
BuildRequires:  python-mo_pack python3-mo_pack
BuildRequires:  scipy >= 0.10, python3-scipy >= 0.10
BuildRequires:  udunits2 >= 2.1.24

# Optional
BuildRequires:  gdal-python >= 1.9.1, gdal-python3 >= 1.9.1
BuildRequires:  graphviz >= 2.18
BuildRequires:  python-filelock, python3-filelock
BuildRequires:  python-mock >= 1.0.1, python3-mock >= 1.0.1
BuildRequires:  python-nose >= 1.1.2, python3-nose >= 1.1.2
BuildRequires:  python-pep8 >= 1.5.7, python3-pep8 >= 1.5.7
BuildRequires:  python-pandas >= 0.11.0, python3-pandas >= 0.11.0
BuildRequires:  python-pillow >= 1.7.8, python3-pillow >= 1.7.8
BuildRequires:  python-shapely >= 1.2.14, python3-shapely >= 1.2.14

%description
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics. The
data model we have chosen is the CF Data Model. The implementation of this
model we have called an Iris Cube.

Iris currently supports read/write access to a range of data formats, including
(CF-)netCDF, GRIB, and PP; fundamental data manipulation operations, such as
arithmetic, interpolation, and statistics; and a range of integrated plotting
options.


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-numpy
Requires:       python2-biggus
Requires:       scipy
Requires:       python2-cartopy
Requires:       pyke
%if 0%{?fedora} > 25
Requires:       python2-netcdf4
%else
Requires:       netcdf4-python
%endif

%description -n python2-%{srcname}
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics. The
data model we have chosen is the CF Data Model. The implementation of this
model we have called an Iris Cube.

Iris currently supports read/write access to a range of data formats, including
(CF-)netCDF, GRIB, and PP; fundamental data manipulation operations, such as
arithmetic, interpolation, and statistics; and a range of integrated plotting
options.


%package -n python3-%{srcname}
Summary:        %{sum}

Requires:       python3-numpy
Requires:       python3-biggus
Requires:       python3-scipy
Requires:       python3-cartopy
Requires:       python3-pyke
%if 0%{?fedora} > 25
Requires:       python3-netcdf4
%else
Requires:       netcdf4-python3
%endif



%description -n python3-%{srcname}
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics. The
data model we have chosen is the CF Data Model. The implementation of this
model we have called an Iris Cube.

Iris currently supports read/write access to a range of data formats, including
(CF-)netCDF, GRIB, and PP; fundamental data manipulation operations, such as
arithmetic, interpolation, and statistics; and a range of integrated plotting
options.


%prep
%autosetup -n %{srcname}-%{version}


%build
echo "[Resources]" > %{iris_sitecfg}
echo "test_data_dir = /usr/share/iris-test-data" >> %{iris_sitecfg}

CFLAGS="%{optflags}" %{__python2} setup.py --with-unpack build
CFLAGS="%{optflags}" %{__python3} setup.py --with-unpack build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%{__python2} setup.py --with-unpack install --skip-build --root %{buildroot}
rm -r %{buildroot}/usr/iris  # Documentation

%{__python3} setup.py --with-unpack install --skip-build --root %{buildroot}
rm -r %{buildroot}/usr/iris  # Documentation


%check
PYTHONPATH="%{buildroot}%{python2_sitearch}" \
	%{__python2} -m iris.tests.runner \
		--default-tests \
		--system-tests \
		--coding-tests \
		--example-tests \
		#--print-failed-images

PYTHONPATH="%{buildroot}%{python3_sitearch}" \
	%{__python3} -m iris.tests.runner \
		--default-tests \
		--system-tests \
		--coding-tests \
		--example-tests \
		#--print-failed-images


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license COPYING COPYING.LESSER
%doc README.md CHANGES CONTRIBUTING.md
%{python2_sitearch}/*
%{python2_sitearch}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license COPYING COPYING.LESSER
%doc README.md CHANGES CONTRIBUTING.md
%{python3_sitearch}/%{srcname}
%{python3_sitearch}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Tue Mar 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.12.0-1
- New upstream release
- Recreate against latest Python spec template
- Update netCDF dependency for Fedora 26

* Mon Oct 31 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.11.0-0.1.rc3
- New upstream release

* Mon Oct 31 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.11.0-0.1.rc2
- New upstream release

* Mon Oct 24 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.11.0-0.1.rc1
- New upstream release
- Enable Python 3 build

* Fri Apr 17 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.8.0-3
- Enable all tests during build
- Print failed images as HTML for debugging

* Fri Apr 17 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.8.0-2
- Fix build path

* Thu Apr 16 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.8.0-1
- Initial RPM release
