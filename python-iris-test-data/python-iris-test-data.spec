%global srcname iris-test-data
%global commit ea0d1b5a2e1f964d5bd91e34101c0cfd95e0d639
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:		python-%{srcname}
Version:	1.12.0
Release:	1%{?dist}
Summary:	Test data for python-iris

Group:		Development/Libraries
License:	LGPLv3
URL:		http://scitools.org.uk/iris/
Source0:	https://github.com/SciTools/%{srcname}/archive/%{commit}/%{name}-%{commit}.tar.gz

BuildArch:	noarch


%description
The Iris library implements a data model to create a data abstraction layer
which isolates analysis and visualisation code from data format specifics.

This package contains test data for the Iris library.


%prep
%setup -q -n %{srcname}-%{commit}


%build


%install
mkdir -p %{buildroot}%{_datadir}/%{srcname}
cp -pR test_data/* %{buildroot}%{_datadir}/%{srcname}/


%files
%doc README.rst
%{_datadir}/%{srcname}


%changelog
* Mon Oct 31 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.11.0-1
- Update for 1.11.0 data

* Sat Oct 1 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.10.0-1
- Update for 1.10.0 data

* Mon Mar 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.9.0-2
- Stop downloading source during build

* Fri Jun 12 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.9.0-1
- Update for 1.9.0 data

* Mon Dec 22 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.3-1
- Update for 1.7.3 data

* Wed Jul 23 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.0-1
- Initial RPM release
