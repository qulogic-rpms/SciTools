%global srcname biggus
%global Srcname Biggus
%global sum Virtual large arrays and lazy evaluation

Name:           python-%{srcname}
Version:        0.15.0
Release:        1%{?dist}
Summary:        %{sum}

License:        LGPLv3
URL:            https://biggus.readthedocs.io
Source0:        https://github.com/SciTools/biggus/archive/v%{version}/%{name}-%{version}.tar.gz
Patch0001:      %{srcname}-0001-TST-Skip-double-ellipsis-test-with-NumPy-1.12.0.patch

BuildArch:      noarch

BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-numpy python3-numpy

# Testing
BuildRequires:  python-pep8 python3-pep8
BuildRequires:  python2-mock python3-mock

# Documentation
BuildRequires:  python3-sphinx

%description
Virtual arrays of arbitrary size, with arithmetic and statistical operations,
and conversion to NumPy ndarrays.

Virtual arrays can be stacked to increase their dimensionality, or tiled to
increase their extent.

Includes support for easily wrapping data sources which produce NumPy ndarray
objects via slicing. For example: netcdf4python Variable instances, and NumPy
ndarray instances.


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-numpy

%description -n python2-%{srcname}
Virtual arrays of arbitrary size, with arithmetic and statistical operations,
and conversion to NumPy ndarrays.

Virtual arrays can be stacked to increase their dimensionality, or tiled to
increase their extent.

Includes support for easily wrapping data sources which produce NumPy ndarray
objects via slicing. For example: netcdf4python Variable instances, and NumPy
ndarray instances.


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-numpy

%description -n python3-%{srcname}
Virtual arrays of arbitrary size, with arithmetic and statistical operations,
and conversion to NumPy ndarrays.

Virtual arrays can be stacked to increase their dimensionality, or tiled to
increase their extent.

Includes support for easily wrapping data sources which produce NumPy ndarray
objects via slicing. For example: netcdf4python Variable instances, and NumPy
ndarray instances.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build

pushd doc
PYTHONPATH="../build/lib" make html SPHINXBUILD=sphinx-build-3 PYTHON=%{__python3}
rm _build/html/.buildinfo
popd
find build/lib -name '*.pyc' -delete
find build/lib -name '__pycache__' -delete


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
%{__python2} setup.py test
%{__python3} setup.py test


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license COPYING COPYING.LESSER
%doc README.rst doc/_build/html
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{Srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license COPYING COPYING.LESSER
%doc README.rst doc/_build/html
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{Srcname}-%{version}-py?.?.egg-info


%changelog
* Tue Mar 7 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.0-1
- New upstream release
- Resync spec with new Python template
- Update netCDF4 dependency on Fedora > 26

* Mon Jul 25 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-2
- Fix version number

* Fri Jul 22 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-1
- New upstream release
- Partial conversion to new template

* Sun Jul 03 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.13.0-2
- Fix build on Fedora 24, NumPy 1.11.0

* Thu Dec 10 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.13.0-1
- New upstream release

* Wed Sep 16 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.0-1
- New upstream release

* Wed Jun 3 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.11.0-1
- New upstream release

* Thu Mar 26 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.0-2
- Patch for tests that fail on 32-bit

* Wed Mar 25 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.0-1
- New upstream release

* Thu Mar 12 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8.0-2
- Fix default integer size in test.

* Tue Jan 6 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8.0-1
- New upstream release

* Sun Jan 4 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.7.0-3
- Add missing BR for testing

* Thu Jan 1 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.7.0-2
- Add checks via testing suite

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.7.0-1
- New upstream release

* Tue Jul 22 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.6.0-1
- Initial RPM release
