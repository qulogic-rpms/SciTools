%if 0%{?fedora} > 12
%global with_python3 1
%else
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib())")}
%endif

%global srcname pyepsg

Name:		python-%{srcname}
Version:	0.3.2
Release:	1%{?dist}
Summary:	A simple interface to http://epsg.io/

License:	LGPLv3
URL:		https://pyepsg.readthedocs.io/
Source0:	https://files.pythonhosted.org/packages/source/p/pyepsg/pyepsg-%{version}.tar.gz

BuildArch:		noarch

BuildRequires:	python2-devel
BuildRequires:	python-setuptools
BuildRequires:	python-sphinx
BuildRequires:	python-requests

%if 0%{?with_python3}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-sphinx
BuildRequires:	python3-requests
%endif # if with_python3

Requires:	python-requests


%description
A simple interface to http://epsg.io/


%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:	A simple interface to http://epsg.io/

Requires:	python3-requests


%description -n python3-%{srcname}
A simple interface to http://epsg.io/
%endif # with_python3


%prep
%setup -q -n %{srcname}-%{version}


%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'


%build
CFLAGS="%{optflags}" %{__python2} setup.py build

pushd docs
make html
rm _build/html/.buildinfo
popd

%if 0%{?with_python3}
pushd %{py3dir}
CFLAGS="%{optflags}" %{__python3} setup.py build

pushd docs
make html
rm _build/html/.buildinfo
popd
popd
%endif # with_python3


%install
# Must do the python3 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python2 version
# to be the default for now).
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif # with_python3

%{__python2} setup.py install --skip-build --root %{buildroot}


%check
%{__python2} -m pyepsg -v

%if 0%{?with_python3}
%{__python3} -m pyepsg -v
%endif


%files
%doc README.rst docs/_build/html
%license COPYING COPYING.LESSER
%{python2_sitelib}/*


%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README.rst docs/_build/html
%license COPYING COPYING.LESSER
%{python3_sitelib}/*
%endif # with_python3


%changelog
* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.2-1
- New upstream release

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.3-1
- New upstream release

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.2-1
- New upstream release

* Tue Jun 7 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.1-1
- New upstream release

* Mon Mar 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-4
- Bump to build again on new Fedora release

* Thu Jul 31 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-3
- Add requests to Requires
- Add tests to check

* Tue Jul 22 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-2
- Document patches better

* Mon Jul 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-1
- Initial RPM release
