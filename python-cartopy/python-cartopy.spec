%global srcname cartopy
%global Srcname Cartopy
%global sum A library providing cartographic tools for Python

Name:		python-%{srcname}
Version:	0.15.1
Release:	1%{?dist}
Summary:	%{sum}

License:	LGPLv3
URL:		http://scitools.org.uk/cartopy/
Source0:	https://github.com/SciTools/%{srcname}/archive/v%{version}/%{srcname}-%{version}.tar.gz

BuildRequires:  geos-devel >= 3.3.3
BuildRequires:  proj-devel >= 4.8.0

BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-setuptools >= 0.7.2, python3-setuptools >= 0.7.2
BuildRequires:  python2-six >= 1.3.0, python3-six >= 1.3.0
BuildRequires:  python2-Cython >= 0.15.1, python3-Cython >= 0.15.1
BuildRequires:  python2-numpy >= 1.6.0, python3-numpy >= 1.6.0
BuildRequires:  python2-matplotlib >= 1.3.0, python3-matplotlib >= 1.3.0
BuildRequires:  python-shapely >= 1.5.6, python3-shapely >= 1.5.6
BuildRequires:  pyshp >= 1.1.4, python3-pyshp >= 1.1.4

# Optional
BuildRequires:  python2-scipy >= 0.10, python3-scipy >= 0.10
BuildRequires:  python-OWSLib >= 0.8.11, python3-OWSLib >= 0.8.11
BuildRequires:  python2-pillow >= 1.7.8, python3-pillow >= 1.7.8
BuildRequires:  gdal-python >= 1.10.0, gdal-python3 >= 1.10.0
BuildRequires:  python-pyepsg >= 0.2.0, python3-pyepsg >= 0.2.0

# Testing
BuildRequires:  python2-mock >= 1.0.1, python3-mock >= 1.0.1
BuildRequires:  python2-nose >= 1.2.1, python3-nose >= 1.2.1
BuildRequires:  python-pep8 >= 1.3.3, python3-pep8 >= 1.3.3

%description
Cartopy is a Python package designed to make drawing maps for data analysis and
visualization easy.

It features:

 * object oriented projection definitions
 * point, line, polygon and image transformations between projections
 * integration to expose advanced mapping in matplotlib with a simple and
   intuitive interface
 * powerful vector data handling by integrating shapefile reading with Shapely
   capabilities


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-six
Requires:       python2-numpy
Requires:       python2-scipy
Requires:       python2-matplotlib
Requires:       python-shapely
Requires:       pyshp
Requires:       python-OWSLib
Requires:       python2-pillow
Requires:       gdal-python
Requires:       python-pyepsg

%description -n python2-%{srcname}
Cartopy is a Python package designed to make drawing maps for data analysis and
visualization easy.

It features:

 * object oriented projection definitions
 * point, line, polygon and image transformations between projections
 * integration to expose advanced mapping in matplotlib with a simple and
   intuitive interface
 * powerful vector data handling by integrating shapefile reading with Shapely
   capabilities


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-six
Requires:       python3-numpy
Requires:       python3-scipy
Requires:       python3-matplotlib
Requires:       python3-shapely
Requires:       python3-pyshp
Requires:       python3-OWSLib
Requires:       python3-pillow
Requires:       gdal-python3
Requires:       python3-pyepsg

%description -n python3-%{srcname}
Cartopy is a Python package designed to make drawing maps for data analysis and
visualization easy.

It features:

 * object oriented projection definitions
 * point, line, polygon and image transformations between projections
 * integration to expose advanced mapping in matplotlib with a simple and
   intuitive interface
 * powerful vector data handling by integrating shapefile reading with Shapely
   capabilities


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python3 version
# to be the default for now).
%py2_install
%py3_install


%files -n python2-%{srcname}
%license COPYING COPYING.LESSER
%doc README.md CHANGES CONTRIBUTING.md
%{python2_sitearch}/%{srcname}
%{python2_sitearch}/%{Srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license COPYING COPYING.LESSER
%doc README.md CHANGES CONTRIBUTING.md
%{python3_sitearch}/%{srcname}
%{python3_sitearch}/%{Srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Mar 11 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.1-1
- New upstream release
- Flatten BRs a bit

* Sat Feb 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.0-1
- New upstream release
- Update spec to latest Python template

* Wed Oct 19 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.3-1
- New upstream release

* Thu Apr 21 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.2-2
- Fix setuptools version

* Wed Apr 20 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.2-1
- New upstream release

* Fri Apr 15 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-3
- Fix plotting in Jupyter notebooks

* Mon Mar 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-2
- Remove gdal-python3 Require for Fedora 22
- Properly enable OWSLib Require for Python 3

* Mon Mar 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-1
- New upstream release

* Wed Jul 1 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.13.0-1
- New upstream release
- Enable OWSLib on Python 3

* Thu Apr 23 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.0-2
- Enable Python 3 build

* Tue Apr 14 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.0-1
- New upstream release

* Tue Aug 26 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.11.2-1
- New upstream release

* Tue Jul 22 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.11.1-1
- Initial RPM release
